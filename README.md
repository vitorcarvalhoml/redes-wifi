
------------------------------------
Modificando a localização dos pontos
------------------------------------

mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                                "MinX", DoubleValue (18.0),
                                "MinY", DoubleValue (18.0),
                                "DeltaX", DoubleValue (5.0),
                                "DeltaY", DoubleValue (10.0),
                                "GridWidth", UintegerValue (3),
                                "LayoutType", StringValue ("RowFirst"));

Modifique os parâmetros "MinX" e "MinY"