#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/netanim-module.h"

// Nossa topologia segue este formato
//
//   Wifi 10.1.3.0
//                 AP
//  *    *    *    *
//  |    |    |    |    10.1.1.0
// n5   n6   n7   n0 -------------- n1   
//                   point-to-point  


using namespace ns3;

static bool g_verbose = true;

/*
 As funções a seguir são utilizadas para rastrear a taxa de transmissão
 com que os pacotes foram enviados.
 Há também uma função para retornar o endereço mac dos nós 
*/

void
DevTxTrace (std::string context, Ptr<const Packet> p)
{
  if (g_verbose)
    {
      std::cout << " TX p: " << *p << std::endl;
    }
}
void
DevRxTrace (std::string context, Ptr<const Packet> p)
{
  if (g_verbose)
    {
      std::cout << " RX p: " << *p << std::endl;
    }
}
void
PhyRxOkTrace (std::string context, Ptr<const Packet> packet, double snr, WifiMode mode, enum WifiPreamble preamble)
{
  if (g_verbose)
    {
      std::cout << "PHYRXOK mode=" << mode << " snr=" << snr << " " << *packet << std::endl;
    }
}
void
PhyRxErrorTrace (std::string context, Ptr<const Packet> packet, double snr)
{
  if (g_verbose)
    {
      std::cout << "PHYRXERROR snr=" << snr << " " << *packet << std::endl;
    }
}
void
PhyTxTrace (std::string context, Ptr<const Packet> packet, WifiMode mode, WifiPreamble preamble, uint8_t txPower)
{
  if (g_verbose)
    {
      std::cout << "PHYTX mode=" << mode << " " << *packet << std::endl;
    }
}
void
PhyStateTrace (std::string context, Time start, Time duration, enum WifiPhy::State state)
{
  if (g_verbose)
    {
      std::cout << " state=" << state << " start=" << start << " duration=" << duration << std::endl;
    }
}

static void
SetPosition (Ptr<Node> node, Vector position)
{
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  mobility->SetPosition (position);
}

static Vector
GetPosition (Ptr<Node> node)
{
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  return mobility->GetPosition ();
}

static void 
AdvancePosition (Ptr<Node> node) 
{
  Vector pos = GetPosition (node);
  pos.x += 5.0;
  if (pos.x >= 210.0) 
    {
      return;
    }
  SetPosition (node, pos);

  if (g_verbose)
    {
      //std::cout << "x="<<pos.x << std::endl;
    }
  Simulator::Schedule (Seconds (1.0), &AdvancePosition, node);
}

std::string
GetMacAddresses (Ptr <Node> n, uint32_t index, bool all)
{
  std::ostringstream oss;
  uint32_t netDeviceCount = n->GetNDevices ();
  if (!all)
  {
    Ptr <NetDevice> nd = n->GetDevice (index);
    Address addr = nd->GetAddress();
    std::ostringstream tempOss;
    tempOss << addr;
    oss << tempOss.str ().substr (6); // exclude first 6 characters of address
  } else {
  for (uint32_t index = 0; index < netDeviceCount; ++index)
    {
      Ptr <NetDevice> nd = n->GetDevice (index);
      Address addr = nd->GetAddress();
      std::ostringstream tempOss;
      tempOss << addr;
      if (tempOss.str ().find("00:00:00:00:00:00") != std::string::npos)
        continue; // exclude loopback
      oss << "*" << tempOss.str ().substr (6); // exclude first 6 characters of address
    }
  }
  return oss.str ();
}

//NS_LOG_COMPONENT_DEFINE ("Programa");

int main (int argc, char *argv[]) {

  // Quantidade de estações criadas
  uint32_t nWifi = 10;
  // Habilitar ou não RTS/CTS (habilitado por padrão)
  uint32_t rts = 1;

  CommandLine cmd;
  cmd.AddValue("rts","Habilitando (true) ou Desabilitando (false) RTS", rts);
  cmd.Parse(argc, argv);
  
  

  // Habilitando alguns logs
  // LogComponentEnable("PacketSink", LOG_LEVEL_ALL);
  // LogComponentEnable("MacLow", LOG_LEVEL_ALL);
  LogComponentEnable("PacketLossCounter", LOG_LEVEL_ALL);
 
  // Habilitando RTS/CTS para a comunicação de pacotes (tamanho acima de 1024)
  if (rts == 1)
  {
    Config::SetDefault("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue("0"));
  } else {
    Config::SetDefault("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue("1024"));  
  }
  
  
  // ================= Configuração Geral AP e Servidor ============================ //
  // Criando nós AP e Servidor
  NodeContainer p2p;
  p2p.Create(2);

  // Criando a conexão entres estes nós
  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute("DataRate", StringValue("5Mbps"));
  pointToPoint.SetChannelAttribute("Delay", StringValue("2ms"));

  // Criando os dispositivos e instalando nos nós
  NetDeviceContainer p2pDevices;
  p2pDevices = pointToPoint.Install(p2p);

  // ================= FIM da Configuração Geral AP e Servidor ===================== //

  // ============== Configuração Geral do AP e das Estações Wifi =================== //
  // Criando nós Estações
  NodeContainer stas;
  stas.Create(nWifi);
  NodeContainer ap = p2p.Get(0); // Fixando variável ap como o nó 0 

  // Criando a estrutura ("ar", canais e camada física)
  YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
  wifiPhy.SetChannel(wifiChannel.Create());

  // Configurando o wifi
  WifiHelper wifi = WifiHelper::Default();
  wifi.SetRemoteStationManager("ns3::AarfWifiManager");

  NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default();

  Ssid ssid = Ssid("ifce-aluno");

  // =========== FIM da Configuração Geral AP e das Estações Wifi ================= //

  // =========== Configuração Geral dos dispositivos dos nós Wifi ================= //
  // Aplicando aos dispositivos wifi a estrutura wifi criada anteriormente
  wifiMac.SetType("ns3::StaWifiMac",
              "Ssid", SsidValue (ssid),
              "ActiveProbing", BooleanValue(false));

  NetDeviceContainer staDevs;
  staDevs = wifi.Install(wifiPhy, wifiMac, stas);

  wifiMac.SetType ("ns3::ApWifiMac",
                   "Ssid", SsidValue(ssid));

  NetDeviceContainer apDevices;
  apDevices = wifi.Install(wifiPhy, wifiMac, ap);

  // ======== FIM da Configuração Geral dos dispositivos dos nós Wifi ============//

  // ======== Configuração Geral da localização dos nós no grid ================= //

  MobilityHelper mobility;
  
  mobility.SetPositionAllocator("ns3::RandomDiscPositionAllocator",
  "X", StringValue("20.0"),
  "Y", StringValue("10.0"),
  "Rho", StringValue("ns3::UniformRandomVariable[Min=0|Max=30]"));

  // Aplicando mobilidade setada as estações
  mobility.Install(stas);

  // Aplicando "mobilidade" ao AP
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install(ap);

  // Fixando os nós AP e Servidor no grid
  AnimationInterface::SetConstantPosition(p2p.Get(1), 22, 44);
  AnimationInterface::SetConstantPosition(p2p.Get(0), 22, 22);  

  // ======= Fim da Configuração Geral da localização dos nós no grid =========== //

  // ================== Configuração Geral da Pilha TCP/IP ====================== //
  // Instalando a Pilha TCP/IP nos nós (Servidor, AP e Estações)
  InternetStackHelper stack;
  stack.Install(p2p.Get(1));
  stack.Install(ap);
  stack.Install(stas);

  // Setando a faixa de IP 192.168.1.0 para AP (ethernet) e Servidor
  Ipv4AddressHelper address;
  address.SetBase ("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaces = address.Assign(p2pDevices);

  // Setando a faixa de IP 192.168.2.0 para AP (wifi) e Estações
  address.SetBase ("192.1.2.0", "255.255.255.0", "0.0.0.100");
  address.Assign (staDevs);
  address.Assign (apDevices);

  // ================== Fim da Configuração Geral da Pilha TCP/IP =============== //
  
  // ============== Configuração Geral das Aplicações dos nós =================== //
  // Configurando a porta que o servidor irá escutar e instalando a Aplicação
  // no nó Servidor
  uint16_t port = 53;
  Address sinkLocalAddress (InetSocketAddress(Ipv4Address::GetAny(), port));
  PacketSinkHelper sinkHelper ("ns3::UdpSocketFactory", sinkLocalAddress);
  ApplicationContainer sinkApp = sinkHelper.Install(p2p.Get(1));
  sinkApp.Start (Seconds (1.0));
  sinkApp.Stop (Seconds (4.0));

  // Criando um Helpr OnOff para ajudar nas aplicações
  OnOffHelper clientHelper ("ns3::UdpSocketFactory", Address());
  clientHelper.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
  clientHelper.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));

  // Criando as aplicações e instalando nas estações wifi
  ApplicationContainer clientApps;
      AddressValue remoteAddress(InetSocketAddress(interfaces.GetAddress(1), port));
      clientHelper.SetAttribute("Remote", remoteAddress);
      clientApps.Add(clientHelper.Install(stas.Get(nWifi - 1)));
      clientApps.Add(clientHelper.Install(stas.Get(nWifi - 2)));
      clientApps.Add(clientHelper.Install(stas.Get(nWifi - 3)));
      clientApps.Add(clientHelper.Install(stas.Get(nWifi - 4)));
      clientApps.Add(clientHelper.Install(stas.Get(nWifi - 5)));
      clientApps.Add(clientHelper.Install(stas.Get(nWifi - 6)));
      clientApps.Add(clientHelper.Install(stas.Get(nWifi - 7)));
      clientApps.Add(clientHelper.Install(stas.Get(nWifi - 8)));
      clientApps.Add(clientHelper.Install(stas.Get(nWifi - 9)));
      clientApps.Add(clientHelper.Install(stas.Get(nWifi - 10)));
  clientApps.Start(Seconds(2.0));
  clientApps.Stop(Seconds(3.0));
   
  // ============== FIM da Configuração Geral das Aplicações dos nós ============ //
   
  // ========================== "Miscelaneous" ================================= //
  AnimationInterface::SetNodeDescription(p2p.Get(0), GetMacAddresses(p2p.Get(0), 1, false)); // Optional

  AnimationInterface::SetNodeDescription(stas.Get(0), GetMacAddresses(stas.Get(0), 0, false)); // Optional
  AnimationInterface::SetNodeDescription(stas.Get(1), GetMacAddresses(stas.Get(1), 0, false)); // Optional
  AnimationInterface::SetNodeDescription(stas.Get(2), GetMacAddresses(stas.Get(2), 0, false)); // Optional
  AnimationInterface::SetNodeDescription(stas.Get(3), GetMacAddresses(stas.Get(3), 0, false)); // Optional
  AnimationInterface::SetNodeDescription(stas.Get(4), GetMacAddresses(stas.Get(4), 0, false)); // Optional
  AnimationInterface::SetNodeDescription(stas.Get(5), GetMacAddresses(stas.Get(5), 0, false)); // Optional
  AnimationInterface::SetNodeDescription(stas.Get(6), GetMacAddresses(stas.Get(6), 0, false)); // Optional
  AnimationInterface::SetNodeDescription(stas.Get(7), GetMacAddresses(stas.Get(7), 0, false)); // Optional
  AnimationInterface::SetNodeDescription(stas.Get(8), GetMacAddresses(stas.Get(8), 0, false)); // Optional
  AnimationInterface::SetNodeDescription(stas.Get(9), GetMacAddresses(stas.Get(9), 0, false)); // Optional

  AnimationInterface::SetNodeColor(ap, 0, 0, 205); // azul 
  AnimationInterface::SetNodeColor(stas, 255, 165, 0); // laranja

  // Imprimindo na tela os endereços IPs dos nós
  cout << "mac address (ap - wifi): " << p2p.Get(0)->GetDevice(1)->GetAddress() << endl;
  cout << "mac address (estacao 1): " << stas.Get(0)->GetDevice(0)->GetAddress() << endl;

  // Gerando tabela de routing
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  // Parando simulação geral no segundo 5
  Simulator::Stop(Seconds(5.0));

  //  Gerando pcap das estações wifi (arquivo wireshark)
  if (rts == 1)
  {
    wifiPhy.EnablePcap("nos-com-rts", staDevs.Get(0), true);
  } else {
    wifiPhy.EnablePcap("nos-sem-rts", staDevs.Get(0), true);  
  }
  
  //  Gerando pcap do servidor (arquivo wireshark)
  if (rts == 1) 
  {
    pointToPoint.EnablePcap("servidor-com-rts", p2pDevices.Get(1), true);  
  } else {
    pointToPoint.EnablePcap("servidor-sem-rts", p2pDevices.Get(1), true);  
  }
 
  // Imprimindo na tela o trace dos pacotes
  Config::Connect("/NodeList/*/DeviceList/*/Mac/MacTx", MakeCallback(&DevTxTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Mac/MacRx", MakeCallback(&DevRxTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Phy/State/RxOk", MakeCallback(&PhyRxOkTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Phy/State/RxError", MakeCallback(&PhyRxErrorTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Phy/State/Tx", MakeCallback(&PhyTxTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Phy/State/State", MakeCallback(&PhyStateTrace));

  // Gerando o arquivo xml com informações extras
  AnimationInterface anim("animacao-com-3-no.xml"); 
  anim.EnablePacketMetadata(true); 

  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
