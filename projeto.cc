#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("Projeto1");

int main(int argc, char *argv[])
{
	// bool verbose = true;
	uint32_t numberWifiNodes = 3;

	/*
	CommandLine cmd;
	cmd.AddValue("numberWifiNodes", "uatalala!", numberWifiNodes);
	cmd.AddValue("verbose", "vou falar a bessa? ", verbose);

	cmd.Parse(argc, argv);
	*/

	// Criando o AP Wifi
	// NodeContainer wifiApNode;
	// wifiApNode.Create (1);

	//Criando o Ponto-a-Ponto entre AP e o Servidor
	NodeContainer APServidorNodes;
	APServidorNodes.Create(2);
 
	// Criando Conexão Ponto-a-Ponto entre o AP e o Servidor
	PointToPointHelper pointToPoint;
	pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
	pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));

	NetDeviceContainer ApServidorDevices;
	ApServidorDevices = pointToPoint.Install(APServidorNodes);

	// Definindo AP
	NodeContainer ap501Node = APServidorNodes.Get(0);
	// Definindo Servidor
	NodeContainer serverNode = APServidorNodes.Get(1);

	// Criando os 4 Nós Estações
	NodeContainer wifiStaNodes;
	wifiStaNodes.Create(numberWifiNodes);

	// Criando o canal Wifi
	YansWifiChannelHelper channel = YansWifiChannelHelper::Default();
  	YansWifiPhyHelper phy = YansWifiPhyHelper::Default();
  	phy.SetChannel(channel.Create());

  	WifiHelper wifi = WifiHelper::Default ();
  	wifi.SetRemoteStationManager("ns3::AarfWifiManager");

  	NqosWifiMacHelper mac = NqosWifiMacHelper::Default();

  	Ssid ssid = Ssid("apto501");
  	mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid), 
  				"ActiveProbing", BooleanValue(false));

  	NetDeviceContainer wifiStaDevices;
  	wifiStaDevices = wifi.Install(phy, mac, wifiStaNodes);

  	mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));

  	NetDeviceContainer APDevice;
  	APDevice = wifi.Install(phy, mac, ap501Node);

  	// Criando Mapeamento do Local
	/*MobilityHelper mobility;
	mobility.SetPositionAllocator("ns3::GridPositionAllocator",
 	"MinX", DoubleValue(0.0),
  	"MinY", DoubleValue(0.0),
 	"DeltaX", DoubleValue(5.0),
 	"DeltaY", DoubleValue(10.0),
 	"GridWidth", UintegerValue(3),
 	"LayoutType", StringValue("RowFirst"));

 	mobility.SetMobilityModel("ns3::RandomWalk2dMobilityModel",
                             "Bounds", 
                             RectangleValue(Rectangle(-50, 50, -50, 50)));
  	mobility.Install(wifiStaNodes);

  	mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  	mobility.Install(ap501Node);*/

	// Criando camada IPV4 e setando endereço na concexão P2P entre AP
	// e servidor
	InternetStackHelper pilha;
	pilha.Install(serverNode);
  	pilha.Install(ap501Node);
  	pilha.Install(wifiStaNodes);

  	Ipv4AddressHelper address;
  	address.SetBase ("192.168.1.0", "255.255.255.0");
  	Ipv4InterfaceContainer interfaces = address.Assign (ApServidorDevices);

  	address.SetBase("192.168.2.0", "255.255.255.0", "0.0.0.3");
  	address.Assign(wifiStaDevices);
  	address.Assign(APDevice);

  	AsciiTraceHelper ascii;
	pointToPoint.EnableAsciiAll(ascii.CreateFileStream("projeto.tr"));

  	Simulator::Run ();
  	Simulator::Destroy ();
  	return 0;
}
