/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/netanim-module.h"


// Default Network Topology
//
//   Wifi 192.168.2.0
//                 AP (apto 501)
//  *    *    *    *
//  |    |    |    |   192.168.1.0
// n5   n6   n7   n0 -------------- server (192.168.1.2)
//                   point-to-point      
//                                                         

using namespace ns3;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("ThirdScriptExample");

void
CourseChange (std::string context, Ptr<const MobilityModel> model) {
  Vector position = model->GetPosition();
  NS_LOG_UNCOND (context <<
  " x = " << position.x << ", y = " << position.y);
}

void MySetPosition(uint16_t X, uint16_t Y, NodeContainer node) {
  MobilityHelper m;
  m.SetPositionAllocator("ns3::GridPositionAllocator",
                                "MinX", DoubleValue(X),
                                "MinY", DoubleValue(Y),
                                "GridWidth", UintegerValue(3),
                                "LayoutType", StringValue ("RowFirst"));

  m.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  m.Install(node);
}

void MySetWifiStaPosition(uint16_t X, uint16_t Y, 
  uint16_t DeltaX, uint16_t DeltaY, NodeContainer node) {

  MobilityHelper m;
  m.SetPositionAllocator("ns3::GridPositionAllocator",
                         "MinX", DoubleValue(X),
                         "MinY", DoubleValue(Y),
                         "DeltaX", DoubleValue(DeltaX),
                         "DeltaY", DoubleValue(DeltaY),
                         "GridWidth", UintegerValue(3),
                         "LayoutType", StringValue("RowFirst"));

  m.SetMobilityModel("ns3::RandomWalk2dMobilityModel",
                     "Bounds", RectangleValue(Rectangle (-50, 50, -50, 50)));
  m.Install(node);
}

int 
main (int argc, char *argv[])
{
  bool verbose = true;
  uint32_t nWifi = 5;
  uint16_t port = 80;
  std::string phyMode("DsssRate1Mbps");
  std::string phyintervalMode("DsssRate1Mbps");
  //uint32_t nPacket = 100;
  /*string fileName = "plot-2d";
  string graphicsFileName        = fileName + ".png";
  string plotFileName            = fileName + ".plt";
  string plotTitle               = "Out x In";
  string dataTitle               = "OutIn Data";

  // Instantiate the plot and set its title.
  Gnuplot plot (graphicsFileName);
  plot.SetTitle (plotTitle);

  // Make the graphics file, which the plot file will create when it
  // is used with Gnuplot, be a PNG file.
  plot.SetTerminal ("png");

  // Set the labels for each axis.
  plot.SetLegend ("Out", "In");

  // Set the range for the x axis.
  plot.AppendExtra ("set xrange [-10:+10]");

  // Instantiate the dataset, set its title, and make the points be
  // plotted along with connecting lines.
  Gnuplot2dDataset dataset;
  dataset.SetTitle (dataTitle);
  dataset.SetStyle (Gnuplot2dDataset::LINES_POINTS);*/

  CommandLine cmd;
  cmd.AddValue("nWifi", "Number of wifi STA devices", nWifi);
  cmd.AddValue("verbose", "Tell echo applications to log if true", verbose);

  cmd.Parse(argc,argv);

  if(nWifi > 18) {
      std::cout << "Number of wifi nodes " << nWifi << 
                   " specified exceeds the mobility bounding box" << std::endl;
      exit (1);
  }

  if(verbose) {
      LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
      LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
  }

  NS_LOG_INFO("Creating 2 nodes (p2p)");

  /* Criando dois nos */
  NodeContainer p2pNodes;
  p2pNodes.Create(2);

  /* Criando o meio que ira conectar os dois nos */
  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute("DataRate", StringValue ("128kbps"));
  pointToPoint.SetChannelAttribute("Delay", StringValue ("2ms"));

  /* Criando os dispositivos aplicado aos dois nos */
  NetDeviceContainer p2pDevices;
  p2pDevices = pointToPoint.Install(p2pNodes);

  /* Criando os nos relacionados a estacoes wifi */
  NodeContainer wifiStaNodes;
  wifiStaNodes.Create(nWifi);

  /* Fixando os nos AP e Server a partir de p2p */
  NodeContainer APNode = p2pNodes.Get(0);
  NodeContainer ServerNode = p2pNodes.Get(1);

  /* Criando o canal "ar" */
  YansWifiChannelHelper channel = YansWifiChannelHelper::Default();
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default();
  phy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
  phy.SetChannel(channel.Create());

  WifiHelper wifi = WifiHelper::Default();

  //wifi.EnableLogComponents();

  //wifi.SetRemoteStationManager("ns3::AarfWifiManager");
  wifi.SetStandard(WIFI_PHY_STANDARD_80211b);
  wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                               "DataMode", StringValue(phyMode),
                               "ControlMode", StringValue(phyMode));

  /* Habilitando log de wifi */

  NqosWifiMacHelper mac = NqosWifiMacHelper::Default();

  // enable RTS/CTS
  UintegerValue ctsThr = 0; //original value:500
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", ctsThr);
  SetRtsCtsThreshold(0);

  Ssid ssid = Ssid("apto501");
  mac.SetType("ns3::StaWifiMac",
              "Ssid", SsidValue(ssid),
              "ActiveProbing", BooleanValue(false)); // sondagem ativa setada para false

  NetDeviceContainer staDevices;
  staDevices = wifi.Install(phy, mac, wifiStaNodes);

  mac.SetType("ns3::ApWifiMac",
              "Ssid", SsidValue(ssid));

  NetDeviceContainer apDevices;
  apDevices = wifi.Install(phy, mac, APNode);

  // Setando posição do AP (X, Y, nó)
  MySetPosition(21, 24, APNode);

  // Setando posição do servidor (X, Y, nó)
  MySetPosition(21, 31, ServerNode);

  // Setando posições da s estações wifi (X, Y, DeltaX, DeltaY, nó)
  MySetPosition(0, 0, wifiStaNodes.Get(0));
  MySetPosition(17, 0, wifiStaNodes.Get(1));
  MySetPosition(34, 0, wifiStaNodes.Get(2));
  MySetPosition(34, 17, wifiStaNodes.Get(3));
  MySetPosition(34, 34, wifiStaNodes.Get(4));
  // MySetW
  ifiStaPosition(16, 16, 21, 21, wifiStaNodes.Get(2));

  

  InternetStackHelper stack;
  stack.Install(ServerNode);
  stack.Install(APNode);
  stack.Install(wifiStaNodes);

  Ipv4AddressHelper address;

  // 192.168.1.1 = AP
  // 192.168.1.2 = Servidor
  address.SetBase("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer p2pInterfaces;
  p2pInterfaces = address.Assign(p2pDevices);

  /*address.SetBase("10.1.2.0", "255.255.255.0");
  Ipv4InterfaceContainer csmaInterfaces;
  csmaInterfaces = address.Assign(csmaDevices);*/

  // 192.168.2.100 = AP
  // 192.168.2.101 = estação
  address.SetBase("192.168.2.0", "255.255.255.0", "0.0.0.100");

  Ipv4InterfaceContainer apInterface;
  apInterface = address.Assign(apDevices);

  Ipv4InterfaceContainer wifiStaInterfaces;
  wifiStaInterfaces = address.Assign(staDevices);

  UdpEchoServerHelper echoServer(port);

  ApplicationContainer serverApps = echoServer.Install(ServerNode);
  serverApps.Start(Seconds(1.0));
  serverApps.Stop(Seconds(10.0));

  // Aplicação envia para servidor (p2pInterfaces.GetAddress(1))
  UdpEchoClientHelper echoClient(p2pInterfaces.GetAddress(1), port);

  cout << "IP do AP (interface wifi): " << apInterface.GetAddress(0) << endl;
  cout << "IP do AP (interface p2p): " << p2pInterfaces.GetAddress(0) << endl;
  cout << "IP do Servidor: " << p2pInterfaces.GetAddress(1) << endl;
  cout << "IP da Estacao 0: " << wifiStaInterfaces.GetAddress(0) << endl;
  cout << "IP da Estacao 1: " << wifiStaInterfaces.GetAddress(1) << endl;
  cout << "IP da Estacao 2: " << wifiStaInterfaces.GetAddress(2) << endl;
  cout << "IP da Estacao 3: " << wifiStaInterfaces.GetAddress(3) << endl;
  cout << "IP da Estacao 4: " << wifiStaInterfaces.GetAddress(4) << endl;


  //UdpEchoClientHelper echoClient("192.168.2.1", port);
  echoClient.SetAttribute("MaxPackets", UintegerValue(1));
  echoClient.SetAttribute("Interval", TimeValue(Seconds(1)));
  echoClient.SetAttribute("PacketSize", UintegerValue(2048));

  /* Aplicação rodando no nó wifi 0 */
  ApplicationContainer clientAppStaWifi0 = 
    echoClient.Install(wifiStaNodes.Get(0));
  clientAppStaWifi0.Start(Seconds(2));
  clientAppStaWifi0.Stop(Seconds(10.0));

  /* Aplicação rodando no nó wifi 1 */
  ApplicationContainer clientAppStaWifi1 = 
    echoClient.Install(wifiStaNodes.Get(1));
  clientAppStaWifi1.Start(Seconds(2));
  clientAppStaWifi1.Stop(Seconds(10.0));

  /* Aplicação rodando no nó wifi 2 */
  ApplicationContainer clientAppStaWifi2 = 
    echoClient.Install(wifiStaNodes.Get(2));
  clientAppStaWifi2.Start(Seconds(2));
  clientAppStaWifi2.Stop(Seconds(10.0));

  /* Aplicação rodando no nó wifi 2 */
  ApplicationContainer clientAppStaWifi3 = 
    echoClient.Install(wifiStaNodes.Get(3));
  clientAppStaWifi3.Start(Seconds(2));
  clientAppStaWifi3.Stop(Seconds(10.0));

  /* Aplicação rodando no nó wifi 2 */
  ApplicationContainer clientAppStaWifi4 = 
    echoClient.Install(wifiStaNodes.Get(4));
  clientAppStaWifi4.Start(Seconds(6.0));
  clientAppStaWifi4.Stop(Seconds(10.0));

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  Simulator::Stop(Seconds(10.0));

  pointToPoint.EnablePcapAll("projeto-final");
  phy.EnablePcap("projeto-final", apDevices.Get(0));
  // csma.EnablePcap("projeto-final", csmaDevices.Get(0), true);

  AsciiTraceHelper ascii;
  pointToPoint.EnableAsciiAll(ascii.CreateFileStream("projeto-final.tr"));

  std::ostringstream oss;
  oss <<
  "/NodeList/" << wifiStaNodes.Get(nWifi - 1)->GetId() <<
  "/$ns3::MobilityModel/CourseChange";
  Config::Connect(oss.str(), MakeCallback(&CourseChange));

  AnimationInterface anim("anim-projeto-final.xml");

  Simulator::Run();
  Simulator::Destroy();
  return 0;
}
