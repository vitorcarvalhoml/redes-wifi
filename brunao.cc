/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/netanim-module.h"


using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("FirstScriptExample");

int
main (int argc, char *argv[])
{

  //uint32_t nCsma = 1;//numero de servidores
  uint32_t nWifi = 3;// numero de estações wifi

  CommandLine cmd;
  cmd.AddValue ("nWifi", "Number of wifi STA devices", nWifi);

  cmd.Parse (argc,argv);
//===========================================================================
  LogComponentEnable ("PacketSink", LOG_LEVEL_ALL);
//===========================================================================

//P2P
//configuração link p2p entre ap e servidor
  NodeContainer p2pNodes;
  p2pNodes.Create (2);

  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("10Mbps"));
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));

  NodeContainer wifiApNode = p2pNodes.Get(0);
  NodeContainer server1 = p2pNodes.Get(1);

  NetDeviceContainer p2pDevices;// contentor de device da rede p2p-P2PDEVICES
  p2pDevices = pointToPoint.Install (p2pNodes);

//=======================================================================
// WIFI
//criação node container das estações
  NodeContainer wifiStaNodes;
  wifiStaNodes.Create (nWifi);

// construção do canal de comunicação wifi entre ap e estações
  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ());

  WifiHelper wifi = WifiHelper::Default ();
  wifi.SetRemoteStationManager ("ns3::AarfWifiManager");

  NqosWifiMacHelper mac = NqosWifiMacHelper::Default (); // RETIRA QOS

// define ssid
  Ssid ssid = Ssid ("ns-3-ssid");
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid),
               "ActiveProbing", BooleanValue (false));

  NetDeviceContainer staDevices; // contentor de dispositivos wifi -STADEVICES
  staDevices = wifi.Install (phy, mac, wifiStaNodes);

  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid));

  NetDeviceContainer apDevices;// contentor do ap--APDEVICES
  apDevices = wifi.Install (phy, mac, wifiApNode);

//=========================================================================
//INSTALADORES DE STACK

  InternetStackHelper stack;
  stack.Install (server1);
  stack.Install (wifiApNode);
  stack.Install (wifiStaNodes);
//======================================================================
//ENDEREÇAMENTO

  Ipv4AddressHelper address;

//endereçamento interface p2p
  address.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer p2pInterfaces;
  p2pInterfaces = address.Assign (p2pDevices);

//endereçamento interface wifi
  address.SetBase ("10.1.3.0", "255.255.255.0");
  address.Assign (apDevices); 
  Ipv4InterfaceContainer staInterfaces;
  staInterfaces = address.Assign (staDevices);
 

//============================================================================
//APLICAÇÕES

//servidor
  UdpEchoServerHelper echoServer (9);

//inicializa servidor
  ApplicationContainer serverApps = echoServer.Install (server1.Get (0));
  serverApps.Start (Seconds (1.0));
  serverApps.Stop (Seconds (10.0));

//cliente
  UdpEchoClientHelper echoClient (staInterfaces.GetAddress (1), 9);
  echoClient.SetAttribute ("MaxPackets", UintegerValue (1));
  echoClient.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
  echoClient.SetAttribute ("PacketSize", UintegerValue (1024));

//inicializa cliente
  ApplicationContainer clientApps = echoClient.Install (wifiStaNodes.Get (0));
  clientApps.Start (Seconds (2.0));
  clientApps.Stop (Seconds (10.0));

//==============================================================================
//MOBILIDADE

  MobilityHelper mobility;

  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "GridWidth", UintegerValue (3),
                                 "LayoutType", StringValue ("RowFirst"));

  mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                             "Bounds", RectangleValue (Rectangle (-50, 50, -50, 50)));
  mobility.Install (wifiStaNodes);

  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wifiApNode);

  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (server1);

//======================================================================================
 AnimationInterface anim ("teste1.xml"); 
  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
