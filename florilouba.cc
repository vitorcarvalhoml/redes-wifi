#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/netanim-module.h"

// Default Network Topology
//
//   Wifi 10.1.3.0
//                 AP
//  *    *    *    *
//  |    |    |    |    10.1.1.0
// n5   n6   n7   n0 -------------- n1   
//                   point-to-point  


using namespace ns3;

static bool g_verbose = true;

void
DevTxTrace (std::string context, Ptr<const Packet> p)
{
  if (g_verbose)
    {
      std::cout << " TX p: " << *p << std::endl;
    }
}
void
DevRxTrace (std::string context, Ptr<const Packet> p)
{
  if (g_verbose)
    {
      std::cout << " RX p: " << *p << std::endl;
    }
}
void
PhyRxOkTrace (std::string context, Ptr<const Packet> packet, double snr, WifiMode mode, enum WifiPreamble preamble)
{
  if (g_verbose)
    {
      std::cout << "PHYRXOK mode=" << mode << " snr=" << snr << " " << *packet << std::endl;
    }
}
void
PhyRxErrorTrace (std::string context, Ptr<const Packet> packet, double snr)
{
  if (g_verbose)
    {
      std::cout << "PHYRXERROR snr=" << snr << " " << *packet << std::endl;
    }
}
void
PhyTxTrace (std::string context, Ptr<const Packet> packet, WifiMode mode, WifiPreamble preamble, uint8_t txPower)
{
  if (g_verbose)
    {
      std::cout << "PHYTX mode=" << mode << " " << *packet << std::endl;
    }
}
void
PhyStateTrace (std::string context, Time start, Time duration, enum WifiPhy::State state)
{
  if (g_verbose)
    {
      std::cout << " state=" << state << " start=" << start << " duration=" << duration << std::endl;
    }
}

static void
SetPosition (Ptr<Node> node, Vector position)
{
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  mobility->SetPosition (position);
}

static Vector
GetPosition (Ptr<Node> node)
{
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  return mobility->GetPosition ();
}

static void 
AdvancePosition (Ptr<Node> node) 
{
  Vector pos = GetPosition (node);
  pos.x += 5.0;
  if (pos.x >= 210.0) 
    {
      return;
    }
  SetPosition (node, pos);

  if (g_verbose)
    {
      //std::cout << "x="<<pos.x << std::endl;
    }
  Simulator::Schedule (Seconds (1.0), &AdvancePosition, node);
}

std::string
GetMacAddresses (Ptr <Node> n, uint32_t index, bool all)
{
  std::ostringstream oss;
  uint32_t netDeviceCount = n->GetNDevices ();
  if (!all)
  {
    Ptr <NetDevice> nd = n->GetDevice (index);
    Address addr = nd->GetAddress();
    std::ostringstream tempOss;
    tempOss << addr;
    oss << tempOss.str ().substr (6); // exclude first 6 characters of address
  } else {
  for (uint32_t index = 0; index < netDeviceCount; ++index)
    {
      Ptr <NetDevice> nd = n->GetDevice (index);
      Address addr = nd->GetAddress();
      std::ostringstream tempOss;
      tempOss << addr;
      if (tempOss.str ().find("00:00:00:00:00:00") != std::string::npos)
        continue; // exclude loopback
      oss << "*" << tempOss.str ().substr (6); // exclude first 6 characters of address
    }
  }
  return oss.str ();
}

//NS_LOG_COMPONENT_DEFINE ("Programa");

int main (int argc, char *argv[]) { 
  
  uint32_t nWifi = 1;
  // LogComponentEnable("PacketSink", LOG_LEVEL_ALL);
  // LogComponentEnable("MacLow", LOG_LEVEL_ALL);
  LogComponentEnable("PacketLossCounter", LOG_LEVEL_ALL);
 
  // enable rts cts all the time.
  Config::SetDefault("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue("0"));
  
  NodeContainer p2p;
  p2p.Create(2);

  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute("DataRate", StringValue("5Mbps"));
  pointToPoint.SetChannelAttribute("Delay", StringValue("2ms"));

  NetDeviceContainer p2pDevices;
  p2pDevices = pointToPoint.Install(p2p);

  NodeContainer stas;
  stas.Create(nWifi);
  NodeContainer ap = p2p.Get(0);

  YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
  wifiPhy.SetChannel(wifiChannel.Create());

  WifiHelper wifi = WifiHelper::Default();
  wifi.SetRemoteStationManager("ns3::AarfWifiManager");

  NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default();

  Ssid ssid = Ssid("ifce-aluno");

  wifiMac.SetType("ns3::StaWifiMac",
              "Ssid", SsidValue (ssid),
              "ActiveProbing", BooleanValue(false));

  NetDeviceContainer staDevs;
  staDevs = wifi.Install(wifiPhy, wifiMac, stas);

  wifiMac.SetType ("ns3::ApWifiMac",
                   "Ssid", SsidValue(ssid));

  NetDeviceContainer apDevices;
  apDevices = wifi.Install(wifiPhy, wifiMac, ap);

  MobilityHelper mobility;
  
  mobility.SetPositionAllocator("ns3::RandomDiscPositionAllocator",
  "X", StringValue("20.0"),
  "Y", StringValue("10.0"),
  "Rho", StringValue("ns3::UniformRandomVariable[Min=0|Max=30]"));
 // mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel","Bounds", RectangleValue (Rectangle (-50, 50, -25, 50)));

  mobility.Install(stas);

  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install(ap);

  AnimationInterface::SetConstantPosition(p2p.Get(1), 22, 44);
  AnimationInterface::SetConstantPosition(p2p.Get(0), 22, 22);  

  InternetStackHelper stack;
  stack.Install(p2p.Get(1));
  stack.Install(ap);
  stack.Install(stas);


  Ipv4AddressHelper address;
  address.SetBase ("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaces = address.Assign(p2pDevices);

  address.SetBase ("192.1.2.0", "255.255.255.0", "0.0.0.100");
  address.Assign (staDevs);
  address.Assign (apDevices);
  
/***************************************************************************/
  uint16_t port = 53;
  Address sinkLocalAddress (InetSocketAddress(Ipv4Address::GetAny(), port));
  PacketSinkHelper sinkHelper ("ns3::UdpSocketFactory", sinkLocalAddress);
  ApplicationContainer sinkApp = sinkHelper.Install (p2p.Get(1));
  sinkApp.Start (Seconds (1.0));
  sinkApp.Stop (Seconds (4.0));

  // Create the OnOff applications to send TCP to the server
  OnOffHelper clientHelper ("ns3::UdpSocketFactory", Address());
  clientHelper.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
  clientHelper.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));

  //normally wouldn't need a loop here but the server IP address is different
  //on each p2p subnet
  ApplicationContainer clientApps;
      AddressValue remoteAddress(InetSocketAddress (interfaces.GetAddress (1), port));
      clientHelper.SetAttribute ("Remote", remoteAddress);
      clientApps.Add(clientHelper.Install(stas.Get (nWifi - 1)));
      // clientApps.Add(clientHelper.Install(stas.Get (nWifi - 2)));
  clientApps.Start(Seconds(2.0));
  clientApps.Stop(Seconds(3.0));
   
   
/*******************************************************************/

  //std::string aux = p2p.Get(0)->GetDevice(1)->GetAddress();
  // AnimationInterface::SetNodeDescription(p2p.Get(0), "MAC Address (wifi): " << p2p.Get(0)->GetDevice(1)->GetAddress()); // Optional
  AnimationInterface::SetNodeDescription(p2p.Get(0), GetMacAddresses(p2p.Get(0), 1, false)); // Optional
  AnimationInterface::SetNodeDescription(stas.Get(0), GetMacAddresses(stas.Get(0), 0, false)); // Optional

  // cout << "mac address (server): " << p2p.Get(1)->GetDevice(0)->GetAddress() << endl;
  // cout << "mac address (ap - ethernet): " << p2p.Get(0)->GetDevice(0)->GetAddress() << endl;
  cout << "mac address (ap - wifi): " << p2p.Get(0)->GetDevice(1)->GetAddress() << endl;
  cout << "mac address (estacao 1): " << stas.Get(0)->GetDevice(0)->GetAddress() << endl;


  // router_0.Get(0)->GetDevice(0)->GetAddress()

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  Simulator::Stop(Seconds(5.0));

  // pointToPoint.EnablePcapAll ("myfirst");
  wifiPhy.EnablePcap("florilouba-wifi", staDevs.Get(nWifi - 1));
  // wifiPhy.EnablePcap ("florilouba-wifi", staDevs.Get(nWifi - 2));
  pointToPoint.EnablePcap("florilouba-p2p", p2pDevices.Get(1), true);
   
  Config::Connect("/NodeList/*/DeviceList/*/Mac/MacTx", MakeCallback(&DevTxTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Mac/MacRx", MakeCallback(&DevRxTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Phy/State/RxOk", MakeCallback(&PhyRxOkTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Phy/State/RxError", MakeCallback(&PhyRxErrorTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Phy/State/Tx", MakeCallback(&PhyTxTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Phy/State/State", MakeCallback(&PhyStateTrace));

  AnimationInterface::SetNodeColor(ap, 0, 255, 0); 
  AnimationInterface::SetNodeColor(stas, 0, 0, 0);
  AnimationInterface anim("florilouba-anim.xml"); 
  anim.EnablePacketMetadata(true); 

  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
