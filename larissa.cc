/*
 * // -*- Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*-
 * script.cc
 *
 *  Created on: 14/09/2012
 *      Author: larissa
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
//#include "ns3/dispatch-module.h"
#include "ns3/ipv4-interface-container.h"
#include "ns3/propagation-module.h"
#include "ns3/applications-module.h"
//#include "ns3/myonoff-application.h"
#include "ns3/flow-monitor-module.h"



#include<iostream>
#include<iomanip>
#include<fstream>
#include<vector>
#include<string>
#include<ctime>
#include<cstdio>
#include<cstdlib>
#include<stdexcept>

NS_LOG_COMPONENT_DEFINE("Script");

using namespace ns3;

static uint32_t m_bytesTotal;

//method that calculates the throughput (Mbps at each 60s)
static void Throughput()
{
double mbps;
Time time;
mbps = (((m_bytesTotal*8.0)/1000000/60));
time = Simulator::Now();
m_bytesTotal = 0;
std::cout<<time.GetSeconds()<<""<<mbps<<std::endl;
Simulator::Schedule(Seconds(60),&Throughput);
}

//method that calculates the receiver payload
void PhyRxOkTrace(std::string conntext, Ptr<const Packet>packet, double snr, WifiMode mode, enum WifiPreamble preamble)
{
Ptr<Packet> m_currentPacket;
WifiMacHeader hdr;
m_currentPacket = packet->Copy();
m_currentPacket->RemoveHeader(hdr);

if(hdr.IsData())
{
m_bytesTotal+=m_currentPacket->GetSize();
- show quoted text -
// enable RTS/CTS
UintegerValue ctsThr = 100; //original value:500
Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", ctsThr);
//non-unicast rate equal to unicast rate
Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode", StringValue(phyMode));
/*Desabilitar fragmentação para quadros menores que 2200 bytes*/
Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("2200"));
- show quoted text -
//Mobility model
MobilityHelper mobility;
Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>(); /*Allocate positions from a deterministic
                                                                                   list specified by the user */

//reads node's position from file
for(int i=0;i<quantNo;i++)
{
int no;
fscanf(top,"%d %lf %lf",&no,&px,&py);
positionAlloc->Add(Vector(px,py,0.0));
}

mobility.SetPositionAllocator(positionAlloc);
mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
mobility.Install(nodes);

// TCP/IP configuration

InternetStackHelper internet;
internet.Install(nodes);

Ipv4AddressHelper ipv4;
NS_LOG_INFO("Assign IP Addresses");
ipv4.SetBase("10.1.1.0","255.255.255.0");
Ipv4InterfaceContainer interface = ipv4.Assign(devices);


/*Cognitive interface configuration*/

ipv4.SetBase("192.1.1.0","255.255.255.0");

WifiHelper cogwifi;

// disable RTS/CTS for every packet
UintegerValue ctsThr2 = 500000; //original value:500
Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", ctsThr2);
Config::SetDefault ("ns3::WifiRemoteStationManager::MaxSlrc", StringValue("0"));


cogwifi.SetStandard (WIFI_PHY_STANDARD_80211b);

// Add a non-QoS upper mac, and disable rate control
NqosWifiMacHelper cogMac = NqosWifiMacHelper::Default ();
//cognitive interface channel number
wifiPhy.Set("ChannelNumber",UintegerValue(6));

YansWifiChannelHelper cogwifiChannel;
cogwifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
cogwifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel","Lambda", DoubleValue (300000000/2.4e9));
wifiPhy.SetChannel (cogwifiChannel.Create ());


cogwifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                          "DataMode",StringValue (phyMode),
                          "ControlMode",StringValue (phyintervalMode));

// Set it to adhoc mode
cogMac.SetType ("ns3::AdhocWifiMac","Sifs",TimeValue(NanoSeconds(26000)));


NetDeviceContainer cogdevices = cogwifi.Install(wifiPhy,cogMac,nodes);
Ipv4InterfaceContainer coginterface = ipv4.Assign(cogdevices);
interface.Add(coginterface);

//Applications
float tempo = 0.01;

for(int i=0; i<=quantEnlace; i++)
{
int from,to;
fscanf(top,"%d %d",&from,&to);
PacketSinkHelper sink("ns3::UdpSocketFactory", InetSocketAddress(interface.GetAddress(to),80));
ApplicationContainer sinkApp = sink.Install(nodes.Get(to));
sinkApp.Start(Seconds(tempo));
sinkApp.Stop(Seconds(50.0));

//install on-off application on main interface
MyOnOffHelper onOff("ns3::UdpSocketFactory", InetSocketAddress(interface.GetAddress(from),80));
//OnOffHelper onOff("ns3::UdpSocketFactory", InetSocketAddress(interface.GetAddress(from),80));
onOff.SetAttribute("OnTime",RandomVariableValue(ConstantVariable(1.0)));
onOff.SetAttribute("OffTime",RandomVariableValue(ConstantVariable(0.0)));
onOff.SetAttribute("PacketSize",UintegerValue(packetSize));
onOff.SetAttribute("Remote",AddressValue(InetSocketAddress(interface.GetAddress(to),80)));

ApplicationContainer udpApp = onOff.Install(nodes.Get(from));

udpApp.Start(Seconds(tempo));
udpApp.Stop(Seconds(50.0));
tempo+=0.2;
}


//Enables the creation of packet tracking files
AsciiTraceHelper ascii;
wifiPhy.EnableAsciiAll(ascii.CreateFileStream("teste1.tr"));


//Tracks the received packets in the chosen terminal
Config::Connect("/NodeList/*/DeviceList/*/Phy/State/RxOk",MakeCallback(&PhyRxOkTrace));
Throughput();

//PCAP files
    wifiPhy.EnablePcap("script",devices);
wifiPhy.EnablePcap("cog-script",cogdevices);

FlowMonitorHelper flowmon;
Ptr<FlowMonitor> monitor = flowmon.InstallAll ();


Simulator::Stop(Seconds(60.1));
Simulator::Run();

//Flow Monitor Code
//Print per flow statistics
 monitor->CheckForLostPackets ();
 Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
 std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
 for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
   {

         Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
         std::cout << "Flow " << i->first  << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";           std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
         std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
         std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / 10.0 / 1024 / 1024  << " Mbps\n";
   }


Simulator::Destroy();

fclose(top);

return 0;
}