#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-module.h"
#include "ns3/athstats-helper.h"
#include "ns3/netanim-module.h"


#include <iostream>

using namespace ns3;

static bool g_verbose = true;

void
DevTxTrace (std::string context, Ptr<const Packet> p)
{
  if (g_verbose)
    {
      std::cout << " TX p: " << *p << std::endl;
    }
}
void
DevRxTrace (std::string context, Ptr<const Packet> p)
{
  if (g_verbose)
    {
      std::cout << " RX p: " << *p << std::endl;
    }
}
void
PhyRxOkTrace (std::string context, Ptr<const Packet> packet, double snr, WifiMode mode, enum WifiPreamble preamble)
{
  if (g_verbose)
    {
      std::cout << "PHYRXOK mode=" << mode << " snr=" << snr << " " << *packet << std::endl;
    }
}
void
PhyRxErrorTrace (std::string context, Ptr<const Packet> packet, double snr)
{
  if (g_verbose)
    {
      std::cout << "PHYRXERROR snr=" << snr << " " << *packet << std::endl;
    }
}
void
PhyTxTrace (std::string context, Ptr<const Packet> packet, WifiMode mode, WifiPreamble preamble, uint8_t txPower)
{
  if (g_verbose)
    {
      std::cout << "PHYTX mode=" << mode << " " << *packet << std::endl;
    }
}
void
PhyStateTrace (std::string context, Time start, Time duration, enum WifiPhy::State state)
{
  if (g_verbose)
    {
      std::cout << " state=" << state << " start=" << start << " duration=" << duration << std::endl;
    }
}

static void
SetPosition (Ptr<Node> node, Vector position)
{
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  mobility->SetPosition (position);
}

static Vector
GetPosition (Ptr<Node> node)
{
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  return mobility->GetPosition ();
}

static void 
AdvancePosition (Ptr<Node> node) 
{
  Vector pos = GetPosition (node);
  pos.x += 5.0;
  if (pos.x >= 210.0) 
    {
      return;
    }
  SetPosition (node, pos);

  if (g_verbose)
    {
      //std::cout << "x="<<pos.x << std::endl;
    }
  Simulator::Schedule (Seconds (1.0), &AdvancePosition, node);
}

NS_LOG_COMPONENT_DEFINE ("Projetofinal");

int main (int argc, char *argv[])
{
  // CommandLine cmd;
  // cmd.AddValue ("verbose", "Print trace information if true", g_verbose);

  // cmd.Parse (argc, argv);

  // Packet::EnablePrinting ();

  // enable rts cts all the time.
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("0"));
  // disable fragmentation
  Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("2200"));


  MobilityHelper mobility;

  // Quantidade de estações
  //uint32_t nWifi = 3;
  //cout << "number nodes: " << nWifi << endl;
  
  // Criando os nós
  NodeContainer stas;
  NodeContainer ap;
  NodeContainer server;
  NodeContainer p2p;

  // Criando os dispositivos
  NetDeviceContainer apDevice;
  NetDeviceContainer p2pDevices;
  NetDeviceContainer staDevs;

  p2p.Create(2);
  ap = p2p.Get(0);
  server = p2p.Get(1);

  stas.Create(2);

  // Criando a ligação entre ap e server
  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute("DataRate", StringValue("5Mbps"));
  pointToPoint.SetChannelAttribute("Delay", StringValue("2ms"));

  // Conectando os dois nós
  p2pDevices = pointToPoint.Install(p2p);

  // Criando camadas de enlace e física (setando canal (6 por exemplo))
  NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default();
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
  YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();
  wifiPhy.SetChannel(wifiChannel.Create());

  // Não tenho muita noção do que se trata essa parte ???
  WifiHelper wifi = WifiHelper::Default ();
  wifi.SetRemoteStationManager ("ns3::ArfWifiManager");

  Ssid ssid = Ssid ("ifce-aluno");

  // Informando as estações para se conectar as camadas anteriormente criadas
  wifiMac.SetType("ns3::StaWifiMac",
                  "Ssid", SsidValue(ssid),
                  "ActiveProbing", BooleanValue(false));
  staDevs = wifi.Install(wifiPhy, wifiMac, stas);
  // Informando ao AP que ele também está na mesma rede que as estações
  wifiMac.SetType ("ns3::ApWifiMac",
                   "Ssid", SsidValue (ssid));
  apDevice = wifi.Install (wifiPhy, wifiMac, ap);

  //Simulator::Schedule (Seconds (1.0), &AdvancePosition, ap.Get (0));

  // Setando posição e movimentação das estações
  mobility.SetPositionAllocator("ns3::RandomDiscPositionAllocator",
    "X", StringValue("5.0"),
    "Y", StringValue("5.0"),
    "Rho", StringValue("ns3::UniformRandomVariable[Min=0|Max=50]"));
  mobility.Install(stas.Get(0));
  
  mobility.SetPositionAllocator("ns3::RandomDiscPositionAllocator",
    "X", StringValue("10.0"),
    "Y", StringValue("10.0"),
    "Rho", StringValue("ns3::UniformRandomVariable[Min=0|Max=50]"));
  mobility.Install(stas.Get(1));

  // Setando posição do servidor
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(ap);
  mobility.Install(server);

  AnimationInterface::SetConstantPosition(p2p.Get(0), 22, 22);
  AnimationInterface::SetConstantPosition(p2p.Get(1), 22, 44);

  // Instalando pilha TCP/IP
  InternetStackHelper stack;
  stack.Install(server);
  stack.Install(ap);
  stack.Install(stas);

  // Configurando interfaces
  Ipv4AddressHelper address;
  Ipv4InterfaceContainer interfaces;
  /*Ipv4InterfaceContainer serverInterface;
  Ipv4InterfaceContainer apInterface;
  */
  address.SetBase ("10.1.1.0", "255.255.255.0");
  interfaces = address.Assign(p2pDevices);
  /*apInterface = address.Assign(apDevice);
  serverInterface = address.Assign(p2pDevices);*/

  address.SetBase("10.1.2.0", "255.255.255.0", "0.0.0.100");
  address.Assign(staDevs);
  address.Assign(apDevice);

  // Preparando as aplicações
  uint16_t port = 53;
  Address sinkLocalAddress (InetSocketAddress (Ipv4Address::GetAny(), port));
  PacketSinkHelper sinkHelper ("ns3::UdpSocketFactory", sinkLocalAddress);
  ApplicationContainer sinkApp = sinkHelper.Install(p2p.Get(1));
  sinkApp.Start (Seconds (1.0));
  sinkApp.Stop (Seconds (3.0));

  OnOffHelper clientHelper("ns3::UdpSocketFactory", Address());
  clientHelper.SetConstantRate(DataRate("500kb/s"));
  clientHelper.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
  clientHelper.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
  
  AddressValue remoteAddress(InetSocketAddress (interfaces.GetAddress(1), port));
  clientHelper.SetAttribute ("Remote", remoteAddress);

  ApplicationContainer clientAppSta0;   
      clientAppSta0.Add(clientHelper.Install(stas.Get(0)));
      //clientAppSta0.Add(clientHelper.Install(stas.Get(1)));
      
      // clientAppSta0.Add(clientHelper.Install(stas.Get(nWifi - 1)));
  clientAppSta0.Start(Seconds(1.0));
  clientAppSta0.Stop(Seconds(5.0));

//ApplicationContainer clientAppSta1;   
  //    clientAppSta1.Add(clientHelper.Install(stas.Get(1)));
      //clientAppSta0.Add(clientHelper.Install(stas.Get(1)));
      
      // clientAppSta0.Add(clientHelper.Install(stas.Get(nWifi - 1)));
 // clientAppSta1.Start(Seconds(1.0));
 // clientAppSta1.Stop(Seconds(5.0));

  /*ApplicationContainer clientAppSta1;
      clientAppSta1.Add(clientHelper.Install(stas.Get(nWifi - 2)));
  clientAppSta1.Start(Seconds(2.0));
  clientAppSta1.Stop(Seconds (6.0));*/

  /*ApplicationContainer apps = onoff.Install (stas.Get (0));
  apps.Start (Seconds (0.5));
  apps.Stop (Seconds (8.0));
*/
  Ipv4GlobalRoutingHelper::PopulateRoutingTables();
  Simulator::Stop(Seconds(10.0));

  //pointToPoint.EnablePcapAll ("myfirst");
  wifiPhy.EnablePcap("Projetofinal", staDevs.Get(0));
  //pointToPoint.EnablePcap ("myfirst", p2pDevices.Get (1), true);

  Config::Connect("/NodeList/*/DeviceList/*/Mac/MacTx", MakeCallback (&DevTxTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Mac/MacRx", MakeCallback (&DevRxTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Phy/State/RxOk", MakeCallback (&PhyRxOkTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Phy/State/RxError", MakeCallback (&PhyRxErrorTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Phy/State/Tx", MakeCallback (&PhyTxTrace));
  Config::Connect("/NodeList/*/DeviceList/*/Phy/State/State", MakeCallback (&PhyStateTrace));

  AthstatsHelper athstats;
  athstats.EnableAthstats ("athstats-sta", stas);
  athstats.EnableAthstats ("athstats-ap", ap);

  AnimationInterface anim("Projetofinal.xml");
  anim.EnablePacketMetadata(true);

  Simulator::Run ();

  Simulator::Destroy ();

  return 0;
}