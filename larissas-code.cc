int main(int argc, char *argv[])
{
FILE *top;
//IEEE 802.11, tx rate = 1Mbps
std::string phyMode("DsssRate1Mbps");
std::string phyintervalMode("DsssRate1Mbps");
//package size in bytes
uint32_t packetSize = 1000;
bool verbose = false;
double px,py;
int quantNo;
int quantEnlace;

//in line command arguments
CommandLine cmd;
cmd.AddValue("phyMode","Wifi Phy Mode", phyMode);
cmd.AddValue("packetSize","Size of the packet to be sent", packetSize);
cmd.AddValue("verbose","Turn on all WifiNetDevice log components", verbose);
cmd.Parse(argc, argv);

/*topology file
* number of nodes	|  number of enlaces  |     -
* node number      |  position x         | position y
* ...              |    ...              |    ...
* transmitter node |  receiver node
*/

//reads the topology file
top = fopen("scratch/topologia.txt","r");
fscanf(top,"%d %d",&quantNo,&quantEnlace);

NS_LOG_INFO ("Create Nodes.");

NodeContainer nodes;
nodes.Create(quantNo);

WifiHelper wifi;

if(verbose)
{
wifi.EnableLogComponents(); //enables wifi log components
}

wifi.SetStandard (WIFI_PHY_STANDARD_80211b);

YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
// set it to zero; otherwise, gain will be added
wifiPhy.Set ("RxGain", DoubleValue (0) );
// ns-3 supports RadioTap and Prism tracing extensions for 802.11b
wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
//main interface channel number
wifiPhy.Set("ChannelNumber",UintegerValue(1));

YansWifiChannelHelper wifiChannel;
wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel","Lambda", DoubleValue (300000000/2.4e9));
wifiPhy.SetChannel (wifiChannel.Create ());

// Add a non-QoS upper mac, and disable rate control
NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
wifi.SetStandard (WIFI_PHY_STANDARD_80211b);
wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                             "DataMode",StringValue (phyMode),
                             "ControlMode",StringValue (phyMode));

// Set it to adhoc mode
wifiMac.SetType ("ns3::AdhocWifiMac");
NetDeviceContainer devices = wifi.Install(wifiPhy,wifiMac,nodes);

// TCP/IP setting

// enable RTS/CTS
UintegerValue ctsThr = 0; //original value:500
Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", ctsThr);
    //non-unicast rate equal to unicast rate
Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode", StringValue(phyMode));

//Mobility model
MobilityHelper mobility;
Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>(); /*Allocate positions from a deterministic
                                                                                   list specified by the user */

//reads node's position from file
for(int i=0;i<quantNo;i++)
{
int no;
fscanf(top,"%d %lf %lf",&no,&px,&py);
positionAlloc->Add(Vector(px,py,0.0));
}

mobility.SetPositionAllocator(positionAlloc);
mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
mobility.Install(nodes);

// TCP/IP configuration

InternetStackHelper internet;
internet.Install(nodes);

Ipv4AddressHelper ipv4;
NS_LOG_INFO("Assign IP Addresses");
ipv4.SetBase("10.1.1.0","255.255.255.0");
Ipv4InterfaceContainer interface = ipv4.Assign(devices);


/*Cognitive interface configuration*/

ipv4.SetBase("192.1.1.0","255.255.255.0");

WifiHelper cogwifi;

cogwifi.SetStandard (WIFI_PHY_STANDARD_80211b);

//cognitive interface channel number
wifiPhy.Set("ChannelNumber",UintegerValue(6));

YansWifiChannelHelper cogwifiChannel;
cogwifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
cogwifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel","Lambda", DoubleValue (300000000/2.4e9));
wifiPhy.SetChannel (cogwifiChannel.Create ());

// Add a non-QoS upper mac, and disable rate control
NqosWifiMacHelper cogMac = NqosWifiMacHelper::Default ();
//cognitive interface channel number
wifiPhy.Set("ChannelNumber",UintegerValue(6));
cogwifi.SetStandard (WIFI_PHY_STANDARD_80211b);
cogwifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                          "DataMode",StringValue (phyMode),
                          "ControlMode",StringValue (phyintervalMode));

// Set it to adhoc mode
cogMac.SetType ("ns3::AdhocWifiMac","Sifs",TimeValue(NanoSeconds(26000)));

// disable RTS/CTS for every packet
UintegerValue ctsThr2 = 500000; //original value:500
Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", ctsThr2);
Config::SetDefault ("ns3::WifiRemoteStationManager::MaxSlrc", StringValue("0"));

NetDeviceContainer cogdevices = cogwifi.Install(wifiPhy,cogMac,nodes);
Ipv4InterfaceContainer coginterface = ipv4.Assign(cogdevices);
interface.Add(coginterface);

//Applications
float tempo = 0.01;
//float cogtempo = 0.03;

for(int i=0; i<quantEnlace; i++)
{
int from,to;
fscanf(top,"%d %d",&from,&to);
PacketSinkHelper sink("ns3::UdpSocketFactory", InetSocketAddress(interface.GetAddress(to),80));
//PacketSinkHelper cogsink("ns3::UdpSocketFactory", InetSocketAddress(coginterface.GetAddress(to),80));

ApplicationContainer sinkApp = sink.Install(nodes.Get(to));
//ApplicationContainer cogsinkApp = cogsink.Install(nodes.Get(to));
sinkApp.Start(Seconds(tempo));
//cogsinkApp.Start(Seconds(cogtempo));
sinkApp.Stop(Seconds(50.0));
//cogsinkApp.Stop(Seconds(51.1));

//install on-off application on main interface
MyOnOffHelper onOff("ns3::UdpSocketFactory", InetSocketAddress(interface.GetAddress(from),80));
onOff.SetAttribute("OnTime",RandomVariableValue(ConstantVariable(1.0)));
onOff.SetAttribute("OffTime",RandomVariableValue(ConstantVariable(1.0)));
onOff.SetAttribute("PacketSize",UintegerValue(packetSize));
onOff.SetAttribute("Remote",AddressValue(InetSocketAddress(interface.GetAddress(to),80)));

//install on-off application on cognitive interface
//MyOnOffHelper cogonOff("ns3::UdpSocketFactory", InetSocketAddress(coginterface.GetAddress(from),80));
//cogonOff.SetAttribute("OnTime",RandomVariableValue(ConstantVariable(1.0)));
//cogonOff.SetAttribute("OffTime",RandomVariableValue(ConstantVariable(1.0)));
//cogonOff.SetAttribute("PacketSize",UintegerValue(packetSize));
//cogonOff.SetAttribute("Remote",AddressValue(InetSocketAddress(coginterface.GetAddress(to),80)));

ApplicationContainer udpApp = onOff.Install(nodes.Get(from));
//ApplicationContainer cogudpApp = cogonOff.Install(nodes.Get(from));

udpApp.Start(Seconds(tempo));
//cogudpApp.Start(Seconds(cogtempo));
udpApp.Stop(Seconds(50.0));
//cogudpApp.Stop(Seconds(51.1));
tempo+=0.2;
}


//Enables the creation of packet tracking files
AsciiTraceHelper ascii;
wifiPhy.EnableAsciiAll(ascii.CreateFileStream("teste1.tr"));


//Tracks the received packets in the chosen terminal
Config::Connect("/NodeListq2/DeviceList/*/Phy/State/RxOk",MakeCallback(&PhyRxOkTrace));
Throughput();

//PCAP files
    wifiPhy.EnablePcap("script",devices);
wifiPhy.EnablePcap("cog-script",cogdevices);


Simulator::Stop(Seconds(60));
Simulator::Run();
Simulator::Destroy();

fclose(top);

return 0;
}

